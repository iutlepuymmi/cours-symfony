## L’installeur symfony

Pour créer une application web traditionnelle :
```
symfony new --webapp my_project
```

Pour créer une app plus légère comme un microservice, une app console ou une API :
```
symfony new my_project
```

Pour installer la version LTS courante :
```
symfony new my_project --version=lts
```

Pour installer la dernière version stable :
```
symfony new my_project --version=stable
```

Pour installer la version de développement :
```
symfony new my_project --version=next
```

et pour installer une version spécifique :
```
symfony new my_project --version=5.4
```

## Avec composer :
Pour créer une application web traditionnelle (ici en version 7) :
```
composer create-project symfony/website-skeleton my_project ^7
```
Pour créer une app plus légère comme un microservice, une app console ou une API (version 7
ici) :
```
composer create-project symfony/skeleton my_project ^7
```

## Squellette d’application Symfony :

```
symfony new --webapp sf-full-project-name
```

La structure du projet skeleton hello-sf ainsi créé est la suivante :
```
dossier-pj/
|-- bin
| |-- console
| `-- phpunit
|-- composer.json
|-- composer.lock
|-- config
| |-- bootstrap.php
| |-- bundles.php
| |-- packages
| |-- routes
| |-- routes.yaml
| `-- services.yaml
|-- phpunit.xml.dist
|-- public
| `-- index.php
|-- src
| |-- Controller
| |-- Entity
| |-- Kernel.php
| |-- Migrations
| `-- Repository
|-- symfony.lock
|-- templates
| `-- base.html.twig
|-- tests
| `-- bootstrap.php
|-- translations
|-- var
| |-- cache
| `-- log
`-- vendor
|-- autoload.php
|-- bin
|-- composer
|-- doctrine
|-- easycorp
|-- egulias
|-- jdorn
|-- monolog
|-- nikic
|-- ocramius
|-- phpdocumentor
|-- psr
|-- sensio
|-- symfony
|-- twig
|-- webmozart
`-- zendframework
```

Le répertoire bin contient l’outil console qui permet d’effectuer les tâches de routine pour créer ou gérer un projet. Le répertoire config contient les fichiers de configuration. Le répertoire public contient le fichier index de l’application Le dossie src les controleurs, le Kernel mais aussi les entités etc. Le dossier var contient les cache et les logs et le dossier vendor les classes des Bundles installés comme http-foundation. 

Vous pouvez consulter le fichier symfony.lock qui se trouve à la racine du dossier  pour voir la liste complète des dépendances installées.

## Débuts avec Symfony

Partons de l'installation précédente avec une full application.

Lançons le serveur web embarqué de Symfony (ici en arrière plan).

```sh
symfony server:start 
```
Pour le stopper :
```
Ctrl+C
```
Visitons 127.0.0.1 :8000 (https ://127.0.0.1 :8000) (dans mon cas) et constatons que Symfony Marche !!