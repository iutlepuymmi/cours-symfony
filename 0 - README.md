# Cours Symfony

Créer une application Symfony

Il existe 2 façons de procéder :
- en utilisant l’installeur Symfony
- en utilisant composer

Utiliser la documentation SY pour vous informer sur les différentes manières.



## la commande bin/console
Cette commande nous permet d’avoir des informations sur notre projet, de faire des actions primaires dessus et de le débugger. Afin d’obtenir la liste des options offertes par cette commande :

```
php bin/console
``````
elle est équivalente à 

```
symfony console
``````
Prenez le temps de lire la documentation de chaque commande et d’essayer de comprendre ce que chacune d’elle fait.
