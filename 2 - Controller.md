## Un Controleur et une route simple

Observer le contenu de routes.yaml dans le dossier config. Laisser le code existant et ajoutezy le code d’un nouveau controleur que nous allons concevoir :

```yml
controllers:
resource:
    path: ../src/Controller/
    namespace: App\Controller
type: attribute
hello:
    path: /hello
    controller: App\Controller\HelloController::sayHello
```

## Controller
Fabriquez un petit contrôleur HelloController dans src/Controller/HelloController.php :

```php
<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelloController extends AbstractController
{

    public function sayHello(): Response
    {
        return new Response("Coucou");
    }
}
```
On teste : 127.0.0.1 :8000/hello (https ://127.0.0.1 :8000/hello)

## Annotations et routes paramétrées
Au lieu de placer toutes les routes de l’application dans un seul fichier, il peut-être plus souple d’utiliser les annotations dans le code même du contrôleur pour plus de commodité.

Recommentez le contenu ajouté dans routes.yaml et décommenter le début permettant de prendre en compte les annotations.

Ajoutons une autre route paramétrée dans notre contrôleur :
```php
<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelloController extends AbstractController
{
    #[Route('/hello', name: 'app_hello')]
    public function index(): Response
    {
        return $this->render('hello/index.html.twig', 
        [
        'controller_name' => 'HelloController',
        ]
        );
    }
    #[Route('/bonjour/{nom}',name: 'app_bonjour')]
    public function bonjour($nom)
    {
        return new Response("Bonjour $nom !");
    }
}
```

## debug des routes
On peut lister toutes ses routes :
```
php bin/console debug:router
```
ou
```
symfony console debug:router
```
on obtient alors tout le listing des routes existantes dans notre application