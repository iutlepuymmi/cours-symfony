## Utiliser des templates Twig
Nous voudrions à présent utiliser des templates Twig dans notre application.

Puis changeons notre contrôleur pour hériter de AbstractController :

```php
<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HelloController extends AbstractController
{
    #[Route('/hello', name: 'app_hello')]
    public function sayHello()
    {
        return new Response('Hello!');
    }

    #[Route('/bonjour/{nom}',name: 'app_bonjour')]
    public function bonjour($nom)
    {
        //return new Response("Bonjour $nom !");
        return $this->render('bonjour.html.twig', [
        'nom' => $nom,
        ]);
    }
}
``````

## template twig
et mettons en place le template correspondant bonjour.html.twig dans le dossier « templates » :

```twig
{# templates/bonjour.html.twig #}
{% extends 'base.html.twig' %}
{% block body %}
<h1>Bonjour</h1>
{% endblock %}
```

avec un base.html.twig du type :

```twig
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block js %}{% endblock %}
    </body>
</html>
```
Retestons : 127.0.0.1 :8000/bonjour/toto (https ://127.0.0.1 :8000/bonjour/toto)

## Memo twig
```
php bin/console debug:twig
```
Vous donnera la liste des Fonctions, Filtres et Tests disponibles dans les templates Twig.