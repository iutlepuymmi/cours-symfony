
## Doctrine et Symfony

ORM signifie Object-Relationnal Mapper. Un ORM sert à offrir une couche d’abstraction de connexion à toutes les BD relationnelles (comme PDO) mais aussi des facilités pour réaliser les requêtes courantes sans descendre au niveau des requêtes SQL et pour générer automatiquement
des entités dans le langage utilisé avec les getters et setters correspondants.

pour disposer de l’installation de l’ORM standard de Symfony qui est Doctrine Voir sa documentation (https ://www.doctrine-project.org/)

## symfony maker-bundle
Il permet de fabriquer des entités, des contrôleurs, des CRUD, des tests etc.

```
php bin/console list make
```
Créons une entité Personne :
```
php bin/console make:entity
```
- Répondez aux questions pour ajouter des champs nom et prenom de type string dans Personne.
- Vérifiez la création du code correspondant dans src/Entity.

Réglons la configuration de la base dans .env sur SQLite :
```
###> doctrine/doctrine-bundle ###
DATABASE_URL=sqlite:///%kernel.project_dir%/var/carnet.db?charset=utf8mb4
###< doctrine/doctrine-bundle ###
```

## Interaction avec la BD

Initialisons la base :
```
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
```

Pour faciliter l’interaction avec la BD, installons easy_admin :
```
composer req admin
```

Puis visitez : localhost :8000/admin (http ://localhost :8000/admin)
Nous obtenons une erreur, complétons donc config/packages/easy_admin.yaml :
```
easy_admin:
entities:
# List the entity class name you want to manage
- App\Entity\Personne

```
et vous pourrez ensuite ajouter facilement des personnes dans l’admin !

## générateur de CRUD
Nous aurions pu aussi utiliser le générateur de CRUD :
```
php bin/console make:crud
```
en choisissant l’entité Personne.

Cela fabrique automatiquement un CRUD avec le contrôleur et les templates nécessaires. Intéressant pour inspecter le code généré même si visuellement easy_admin est plus abouti …

Remarquez la commande que nous avons utilisée pour lister les routes de notre projet : debug :router Displays current routes for an application

Puis retester : 127.0.0.1 :8000/bonjour/toto (https ://127.0.0.1 :8000/bonjour/toto)



## profiler
Pour tester le CRUD et le profiler : localhost :8000/personne (http ://localhost :8000/personne).

Vous pouvez visualiser le Profiler (barre noire en bas de l'écran). Il permet d'effectuer des Debugs tout au long d'un projet Symfony.

## debug des routes
On peut lister toutes les routes :
```
php bin/console debug:router
```
Vous permettra de visualiser toutes les routes de l'application

## Interaction avec la BD
On lance le makemigrations suivi du migrate dans le cas d'une application en production permettant d'avoir un suivi détaillé des migrations de la DB au fur et à mesure de la vie de l'application.

```
php bin/console make:migration
php bin/console doctrine:migrations:migrate
```
En cas de problème, ou pour aller plus vite dans les phases de développement on peut forcer la synchonisation du nouveau schéma de BD, et éviter les migrations au départ, tant que la V1 de l'application n'est pas finalisé.
```
php bin/console doctrine:schema:update --force
```
Voir la doc correspondante (https ://symfony.com/doc/current/doctrine/associations.html)

(Si easy_admin est installé, ajoutons un ou 2 auteurs puis livres dans easy_admin (http ://localhost :8000/admin) )

## Relançons le make :crud

```
php bin/console make:crud
```
pour les entités  puis améliorons un peu les templates proposés.

## Template de base avec Bootstrap
On peut déjà améliorer le template de base base.html.twig avec Bootstrap, ici Bootswatch (https ://bootswatch.com/), ou tout autre librairie CSS

Si besoin, ajoutez une méthode __toString() aux entités :
```php
<?php
...
public function __toString()
{
    return $this->getPrenom() . ' ' . $this->getNom();
}
...
```
