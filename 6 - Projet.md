Vous êtes développeur et souhaiter créer une plateforme de gestion pour des collectionneurs.

L'équipe souhaite créer un nouveau service pour ses utilisateurs. Vous êtes libre du choix du type de collection.

La plateforme permettra d'ajouter une collection avec possibilité de notes et d'avis .

​
Vous êtes en charge du développement de cette première version.

​
Concernant les spec techniques (Front):

- Un utilisateur pourra se connecter/s'inscrire.
- Un utilisateur pourra ajouter un produit à sa collection depuis une liste prédéfinie.
- Un utilisateur pourra donner un avis et mettre une note sur 5 étoiles
- Un utilisateur pourra afficher sa liste de produit, les ordonner par nom ou notes et les filtrer par genre.
​

Concernant les spec techniques (back):

- Un admin pourra se connecter à la partie administration du site.
- Un admin pourra consulter et modérer les avis (CRUD)
- Un admin pourra gérer la liste des produits (CRUD)
- Un admin pourra gérer la liste des catégories (CRUD)
jeux => MMO / FPS / ...
Voitures => Ancienne / Moderne / Sportive / ...
Timbre => Rare / Français / Régional ...
Un admin pourra définir dans quel catégorie est associé le produit
​

Un produit/contenu sera composé au minimum de :

- un titre (obligatoire)
- une description (obligatoire)
- une image de couverture (non obligatoire)
- une date de création (obligatoire)
- une note moyenne des avis (non obligatoire)
- une ou plusieurs catégorie (obligatoire)
​

Une catégorie sera composée de :

- un titre (obligatoire)
- une description (non obligatoire)
- un logo ou image (non obligatoire)
​

Un avis sera composé de :

- nom de l'utilisateur connecté (obligatoire)
- une date de création (obligatoire)
- une note de 1 à 5 étoiles (obligatoire)
- un descriptif d'avis (obligatoire)
- un état de modération de l'avis
validé (par défaut)
suspendu
​

Un utilisateur sera composé de :

- un pseudo
- un email (obligatoire)
- un password (obligatoire)
- un tableau de rôles (obligatoire)
​

Exemple du CRUD d'un genre :

- GET "/admin/category" -> liste les genres
- GET "/admin/category/{id} -> affiche un genre
- POST "/admin/category" -> affiche le form d'ajout et le traite pour ajouter un genre
- PUT "/admin/category/{id}" -> affiche le form pour modifier un genre
- DELETE "/admin/category/{id} -> supprime le genre choisi
​

En Bonus :
- Fixtures fonctionnelles
- Datas générées par Faker

Conseil : TOUT EST DANS LA DOCUMENTATION SYMFONY !

