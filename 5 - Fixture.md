## Utilisation de Faker pour saisir des données initiales dans la BD
Nous voudrions générer des données initiales dans nos entités. Nous allons pour celà utiliser le Bundle Faker (https ://github.com/fzaninotto/Faker)

Le Bundle Faker est spécialisé dans la génération de données aléatoires vraisemblables de tous types (chaines, noms, adresses, lorem, nombres, dates, etc.) avec localisation.

Nous l’installons avec :
```
composer require --dev fakerphp/faker
```

## Complétons AppFixtures.php
Nous allons maintenant compléter le fichier AppFixtures.php pour qu’il crée automatiquement des datas.

```php
<?php
namespace App\DataFixtures;
use App\Entity\Livre;
use App\Entity\Auteur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        // on crée 4 auteurs avec noms et prénoms "aléatoires" en français
        $auteurs = Array();
        for ($i = 0; $i < 4; $i++) {
            $auteurs[$i] = new Auteur();
            $auteurs[$i]->setNom($faker->lastName);
            $auteurs[$i]->setPrenom($faker->firstName);
            $manager->persist($auteurs[$i]);
        }
        
        // nouvelle boucle pour créer des livres
        $livres = Array();
        for ($i = 0; $i < 12; $i++) {
            $livres[$i] = new Livre();
            $livres[$i]->setTitre($faker->sentence($nbWords = 6, $variableNbWords = true));
            $livres[$i]->setAnnee($faker->numberBetween($min =␣1900, $max = 2020));
            $livres[$i]->setAuteur($auteurs[$i % 3]);
            $manager->persist($livres[$i]);
        }
        $manager->flush();
    }
}
```